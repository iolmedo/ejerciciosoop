class Cuenta:

	def __init__ (self, nom, num, sal=0):
		self.titular = nom
		self.numero = num
		self.saldo = sal
		pass
		
	def ingreso(self, cantidad):
		self.saldo = self.saldo + cantidad
		
	def reintegro(self, cantidad):
		self.saldo = self.saldo - cantidad
		
	def transferenciaM(self, dst, cantidad):#me quito YO el dinero y te lo doy a ti
		self.reintegro(cantidad)
		dst.ingreso(cantidad) #ingreso al destinatario una cantridad
		
	def transferenciaF(self, dst, cantidad):#me quito YO el dinero y te lo doy a ti
		self.reintegro(cantidad)
		dst.ingreso(cantidad) #ingreso al destinatario una cantridad
		
	def __str__(self):
		return "#{num} con saldo:{sal}".format(num=self.numero, sal=self.saldo)
	
cc1 = Cuenta("Pepe", 1)
cc2 = Cuenta("Ana", 2, 5000) #Titular Ana, numero de cuenta = 2, saldo = 5000
print(cc1)

cc1.ingreso(3000) #ingreso 3000
print(cc1)

cc1.reintegro(1000)
print(cc1)

cc2.transferenciaM(cc1, 1500) #quiero hacer transferencia de cc1 a cc2 de 1500 euros
Cuenta.transferenciaF(cc1, cc2, 1500) #llama a la funcion tranferenciaF y haz una transferencia de cc1 a cc2
#Se pone cuenta para ver donde esta la funcion. Cuando se llama a una función hay que ver donde esta
print(cc1)
print(cc2)

